//
//  AGTConcurrenceViewController.m
//  Concurrencia
//
//  Created by Fernando Rodríguez Romero on 20/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTConcurrenceViewController.h"

@interface AGTConcurrenceViewController ()

@end

@implementation AGTConcurrenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)syncDownload:(id)sender {
    
    NSURL *url = [NSURL URLWithString:@"http://cdn.brosome.com/wp-content/uploads/2011/10/Catalina-Otalvaro-Besame-lingerie-5.jpg"];
    NSData *imgData = [NSData dataWithContentsOfURL:url];
    UIImage *img = [UIImage imageWithData:imgData];
    
    self.photoView.image = img;
    
    
}

- (IBAction)asyncDownload:(id)sender {
    
    
    // creamos una cola
    dispatch_queue_t download = dispatch_queue_create("download", NULL);
    
    
    // le enviamos un bloque
    dispatch_async(download, ^{
        NSURL *url = [NSURL URLWithString:@"http://elhorizonte.mx/foto/470000/475983_6.jpg"];
        NSData *imgData = [NSData dataWithContentsOfURL:url];
        UIImage *img = [UIImage imageWithData:imgData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.photoView.image = img;
        });
        
    });
    
    
}

- (IBAction)asyncProDownload:(id)sender {
    
    [self withImage:^(UIImage *image) {
        // Piensa mal y te quedarás corto
        dispatch_async(dispatch_get_main_queue(), ^{
            self.photoView.image = image;
        });
        
        
    }];
}




-(void)withImage:(void (^)(UIImage *image))completionBlock{
    
    // Envio un bloque a una cola de sistema
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        // descargo a la gemela en segundo plano
        NSURL *url = [NSURL URLWithString:@"http://img138.imageshack.us/img138/1714/marianaandcamiladavalos.jpg"];
        NSData *imgData = [NSData dataWithContentsOfURL:url];
        UIImage *img = [UIImage imageWithData:imgData];
        
        // una vez que la tengo....se la paso al bloque de finalización
        // ¡en la cola principal!
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock(img);
        });
        

    });
    
    
    
    
    
}
@end














