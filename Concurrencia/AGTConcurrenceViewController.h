//
//  AGTConcurrenceViewController.h
//  Concurrencia
//
//  Created by Fernando Rodríguez Romero on 20/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTConcurrenceViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
- (IBAction)syncDownload:(id)sender;
- (IBAction)asyncDownload:(id)sender;
- (IBAction)asyncProDownload:(id)sender;

@end
